/**
 * Credenciales de Mongo Atlas
 * 
 * user:admin
 * password:zGfV7qzvpbkTI8mA
 * 
 * 
 * Mongo DB
 * mongodb+srv://admin:zGfV7qzvpbkTI8mA@cluster0-j3umb.mongodb.net/cafe

 * */

require('./config/config');
const express = require('express');
const mongoose = require('mongoose');

const app = express();
const bodyParser = require('body-parser');
const path = require('path');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//habilitar la carpeta public
app.use(express.static( path.resolve(__dirname, '../public')));

//Configuracion global de rutas
app.use(require('./routes/index'));

mongoose.connect(process.env.URL_DB, { useNewUrlParser: true, useCreateIndex: true }, (err, res) => {
    if (err) throw err;
    console.log("Base online");
});

app.listen(process.env.PORT, () => {
    console.log(`Escuchando puerto  ${process.env.PORT}`);
});